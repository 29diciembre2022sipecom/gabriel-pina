import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './pages/header/header.components';
import { ContentComponent } from './pages/content/content.components';
import { FooterComponent} from './pages/footer/footer.components';
//import { DirectivaComponent } from './directiva/directiva.component';
//import { ClientesComponent } from './clientes/clientes.component';
//import { ClienteService } from './clientes/cliente.service';
//import { RouterModule, Routes} from '@angular/router';
//import { HttpClientModule } from '@angular/common/http';
//import { FormComponent } from './clientes/form.component';
import { ProductsComponent } from './pages/products/products.component';
import { LoginComponent } from './pages/login/login.components';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ProductService } from './pages/products/products.service';

const routes: Routes = [
  {path: '', redirectTo: './content', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'products', component: ProductsComponent}
  //{path: '', redirectTo: '/clientes', pathMatch: 'full'},
  //{path: 'directivas', component: DirectivaComponent},
  //{path: 'clientes', component: ClientesComponent},
  //{path: 'clientes/form', component: FormComponent},
  //{path: 'clientes/form/:id', component: FormComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    ProductsComponent,
    FooterComponent,
    LoginComponent
    //DirectivaComponent,
    //ClientesComponent,
    //FormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
    //HttpClientModule,
    //FormsModule,
  ],
  //providers: [ClienteService],
  providers: [ProductService],
  bootstrap: [AppComponent]
})

export class AppModule { }

