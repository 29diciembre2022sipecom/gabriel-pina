package com.sipecom.tienda.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sipecom.tienda.models.OrdersModels;
import com.sipecom.tienda.repository.OrdersRepository;

@Service
public class OrdersServiceImp {
	
	@Autowired
	private OrdersRepository ordersRepository;
	
	public List<OrdersModels> findAll() {
		return (List<OrdersModels>) ordersRepository.findAll();
	}

	
	public void save(OrdersModels clients) {
		ordersRepository.save(clients);
	}

	public OrdersModels findById(Long id) {
		return ordersRepository.findById(id).orElse(null);
	}

	
	public void delete(OrdersModels clients) {
		ordersRepository.delete(clients);
	}

}
