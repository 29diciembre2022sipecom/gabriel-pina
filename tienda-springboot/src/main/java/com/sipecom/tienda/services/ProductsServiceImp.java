package com.sipecom.tienda.services;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sipecom.tienda.models.ProductsModels;
import com.sipecom.tienda.repository.ProductsRepository;

@Service
public class ProductsServiceImp{
	
	String urlApi = "https://fakestoreapi.com/products";
	
	
	@Autowired
	private ProductsRepository productRepo;
	
	public String saveList() {
		productRepo.saveAll(loadarticles());
		return "Articulos Arriba";
	}
	
	public List<ProductsModels> findAll() {
		return (List<ProductsModels>) productRepo.findAll();
	}

	
	public void save(ProductsModels products) {
		productRepo.save(products);
	}

	public ProductsModels findById(Long id) {
		return productRepo.findById(id).orElse(null);
	}

	
	public void delete(ProductsModels products) {
		productRepo.delete(products);
	}
	
	
	public  List<ProductsModels> loadarticles() {
		RestTemplate apiRest = new RestTemplate();
		 ResponseEntity<ProductsModels[]> response =
				 apiRest.getForEntity(
						 urlApi,
						 ProductsModels[].class);
		 ProductsModels[] artc = response.getBody();
              List<ProductsModels> ar = Arrays.asList(artc);
              
      return  ar;
	}

}
