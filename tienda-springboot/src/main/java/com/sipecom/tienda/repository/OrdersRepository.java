package com.sipecom.tienda.repository;

import org.springframework.data.repository.CrudRepository;

import com.sipecom.tienda.models.OrdersModels;

public interface OrdersRepository extends CrudRepository<OrdersModels, Long>{

}
