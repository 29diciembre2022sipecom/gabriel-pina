package com.sipecom.tienda.repository;

import org.springframework.data.repository.CrudRepository;

import com.sipecom.tienda.models.ProductsModels;

public interface ProductsRepository extends CrudRepository<ProductsModels, Long>{

}
