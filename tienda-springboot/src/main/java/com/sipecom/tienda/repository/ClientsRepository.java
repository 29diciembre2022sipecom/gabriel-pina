package com.sipecom.tienda.repository;

import org.springframework.data.repository.CrudRepository;

import com.sipecom.tienda.models.ClientsModels;

public interface ClientsRepository extends CrudRepository<ClientsModels, Long>{

}
