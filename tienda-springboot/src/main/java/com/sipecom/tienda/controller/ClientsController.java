package com.sipecom.tienda.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sipecom.tienda.models.ClientsModels;

import com.sipecom.tienda.services.ClientsServiceImp;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class ClientsController {
	
	@Autowired
	private ClientsServiceImp clientservice;
	
	@GetMapping("/clients")
	public List<ClientsModels> index() {
		return clientservice.findAll();
	}

	@GetMapping("/clients/{id}")
	public ClientsModels show(@PathVariable Long id) {
		return this.clientservice.findById(id);
	}

	@PostMapping("/clients")
	@ResponseStatus(HttpStatus.CREATED)
	public ClientsModels create(@RequestBody ClientsModels clients) {
		this.clientservice.save(clients);
		return clients;
	}

	@PutMapping("/clients/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ClientsModels update(@RequestBody ClientsModels clients, @PathVariable Long id) {
		ClientsModels currentClients = this.clientservice.findById(id);
		currentClients.setUsername(clients.getUsername());
		currentClients.setPassword(clients.getPassword());
		currentClients.setName(clients.getName());
		currentClients.setNumber(clients.getNumber());
		currentClients.setDireccion(clients.getDireccion());
		this.clientservice.save(currentClients);
		return currentClients;
	}

	@DeleteMapping("/clients/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		ClientsModels currentClients = this.clientservice.findById(id);
		this.clientservice.delete(currentClients);
	}

}
