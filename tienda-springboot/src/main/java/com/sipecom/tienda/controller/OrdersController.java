package com.sipecom.tienda.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import com.sipecom.tienda.models.OrdersModels;
import com.sipecom.tienda.services.OrdersServiceImp;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class OrdersController {
	
	private OrdersServiceImp orderservice;
	
	@GetMapping("/orders")
	public List<OrdersModels> index() {
		return orderservice.findAll();
	}

	@GetMapping("/orders/{id}")
	public OrdersModels show(@PathVariable Long id) {
		return this.orderservice.findById(id);
	}

	@PostMapping("/orders")
	@ResponseStatus(HttpStatus.CREATED)
	public OrdersModels create(@RequestBody OrdersModels orders) {
		this.orderservice.save(orders);
		return orders;
	}

	@PutMapping("/orders/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public OrdersModels update(@RequestBody OrdersModels orders, @PathVariable Long id) {
		OrdersModels currentOrders = this.orderservice.findById(id);
		currentOrders.setArticle(orders.getArticle());
		currentOrders.setDescripcion(orders.getDescripcion());
		currentOrders.setCantidad(orders.getCantidad());
		this.orderservice.save(currentOrders);
		return currentOrders;
	}

	@DeleteMapping("/orders/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		OrdersModels currentOrders = this.orderservice.findById(id);
		this.orderservice.delete(currentOrders);
	}

}
