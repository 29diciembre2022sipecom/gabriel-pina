package com.sipecom.tienda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiendaSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiendaSpringbootApplication.class, args);
	}

}
