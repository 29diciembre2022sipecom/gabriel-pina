package org.example;

import java.security.SecureRandom;
import java.util.Random;

public class algoritmo2 {
    public static String generarRandomPasswordv2(int len)
    {
        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!()=$#&/%";

        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < len; i++)
        {
            int randomIndex = random.nextInt(chars.length());
            sb.append(chars.charAt(randomIndex));
        }

        return sb.toString();
    }

    public static void main(String[] args)
    {
        int len_min = 8;
        int len_max = 15;
        Random lenR = new Random();
        int len = lenR.nextInt(len_max) + len_min;
        System.out.println(generarRandomPasswordv2(len));
    }
}

